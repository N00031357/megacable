﻿using MegaCable.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MegaCable.Entities;
using MegaCable.Repositories.Configurations;
using System.Data.Entity;

namespace MegaCable.Services
{
    public class CobranzaService : ICobranzaService
    {
        private DbEntities entities;

        public CobranzaService(DbEntities entities)
        {
            this.entities = entities;
        }

        public void Editar(Cobranza cobranza)
        {
            entities.Entry(cobranza).State = EntityState.Added;
            entities.Entry(cobranza).State = EntityState.Modified;
            entities.SaveChanges();
        }

        public Cobranza GetCobranza(int id)
        {
            return entities.Cobranzas.Find(id);
        }

        public IEnumerable<Cobranza> GetCobranza()
        {
            return entities.Cobranzas;
        }

        public void Guardar(Cobranza cobranza)
        {
            entities.Cobranzas.Add(cobranza);
            entities.SaveChanges();
        }
    }
}
