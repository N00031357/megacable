﻿using MegaCable.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MegaCable.Entities;
using MegaCable.Repositories.Configurations;
using System.Data.Entity;

namespace MegaCable.Services
{
    public class ContratoService : IContratoService
    {
        private DbEntities entities;

        public ContratoService(DbEntities entities)
        {
            this.entities = entities;
        }

        public void Editar(Contrato contrato)
        {
            entities.Entry(contrato).State = EntityState.Added;
            entities.Entry(contrato).State = EntityState.Modified;
            entities.SaveChanges();
        }

        public Contrato GetContrato(int id)
        {
            return entities.Contratos.Find(id);
        }

        public IEnumerable<Contrato> GetContrato()
        {
            return entities.Contratos;
        }

        public void Guardar(Contrato contrato)
        {
            entities.Contratos.Add(contrato);
            entities.SaveChanges();
        }
    }
}
