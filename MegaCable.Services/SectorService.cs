﻿using MegaCable.Interfaces;
using MegaCable.Repositories.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MegaCable.Entities;

namespace MegaCable.Services
{
    public class SectorService : ISectorService
    {
        private DbEntities entities;

        public SectorService(DbEntities entities)
        {
            this.entities = entities;
        }

        public List<Sector> GetSectores()
        {
            return entities.Sectores.ToList();
        }
    }
}
