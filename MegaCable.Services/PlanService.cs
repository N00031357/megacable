﻿using MegaCable.Interfaces;
using MegaCable.Repositories.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MegaCable.Entities;

namespace MegaCable.Services
{
    public class PlanService : IPlanService
    {
        private DbEntities entities;

        public PlanService(DbEntities entities)
        {
            this.entities = entities;
        }

        public List<Plan> GetPlanes()
        {
            return entities.Planes.ToList();
        }
    }
}
