﻿using MegaCable.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MegaCable.Entities;
using MegaCable.Repositories.Configurations;
using System.Data.Entity;

namespace MegaCable.Services
{
    public class ReciboService : IReciboService
    {
        private DbEntities entities;

        public ReciboService(DbEntities entities)
        {
            this.entities = entities;
        }

        public void Editar(Recibo recibo)
        {
            entities.Entry(recibo).State = EntityState.Added;
            entities.Entry(recibo).State = EntityState.Modified;
            entities.SaveChanges();
        }

        public Recibo GetRecibo(int id)
        {
            return entities.Recibos.Find(id);
        }

        public IEnumerable<Recibo> GetRecibo()
        {
            return entities.Recibos;
        }

        public void Guardar(Recibo recibo)
        {
            entities.Recibos.Add(recibo);
            entities.SaveChanges();
        }
    }
}
