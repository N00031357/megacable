﻿using MegaCable.Entities;
using MegaCable.Repositories.Configurations.Map;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Repositories.Configurations
{
    public class DbEntities : DbContext
    {
        public virtual IDbSet<Abonado> Abonados { get; set; }
        public virtual IDbSet<Cobranza> Cobranzas { get; set; }
        public virtual IDbSet<Contrato> Contratos { get; set; }
        public virtual IDbSet<Plan> Planes { get; set; }
        public virtual IDbSet<Recibo> Recibos { get; set; }
        public virtual IDbSet<Sector> Sectores { get; set; }
        public virtual IDbSet<UbigeoUnico> Ubigeos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new AbonadoMap());
            modelBuilder.Configurations.Add(new CobranzaMap());
            modelBuilder.Configurations.Add(new ContratoMap());
            modelBuilder.Configurations.Add(new PlanMap());
            modelBuilder.Configurations.Add(new ReciboMap());
            modelBuilder.Configurations.Add(new SectorMap());
            modelBuilder.Configurations.Add(new UbigeoUnicoMap());
        }
    }
}
