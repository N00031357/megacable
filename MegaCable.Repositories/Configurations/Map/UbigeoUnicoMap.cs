﻿using MegaCable.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Repositories.Configurations.Map
{
    public class UbigeoUnicoMap : EntityTypeConfiguration<UbigeoUnico>
    {
        public UbigeoUnicoMap()
        {
            ToTable("Ubigeos");
            HasKey(o => o.Id);
            Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
