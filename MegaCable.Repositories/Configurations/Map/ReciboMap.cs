﻿using MegaCable.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Repositories.Configurations.Map
{
    public class ReciboMap : EntityTypeConfiguration<Recibo>
    {
        public ReciboMap()
        {
            ToTable("Recibos");
            HasKey(o => o.Id);
            Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(p => p.Contrato).WithMany().HasForeignKey(t => t.ContratoId);
        }
    }
}
