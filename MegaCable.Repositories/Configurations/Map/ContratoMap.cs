﻿using MegaCable.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Repositories.Configurations.Map
{
    public class ContratoMap : EntityTypeConfiguration<Contrato>
    {
        public ContratoMap()
        {
            ToTable("Contratos");
            HasKey(o => o.Id);
            Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(p => p.Abonado).WithMany().HasForeignKey(t => t.AbonadoId);
            HasRequired(p => p.Plan).WithMany().HasForeignKey(t => t.PlanId);
            HasRequired(p => p.Sector).WithMany().HasForeignKey(t => t.SectorId);
        }
    }
}
