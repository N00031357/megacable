﻿using MegaCable.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Repositories.Configurations.Map
{
    public class CobranzaMap : EntityTypeConfiguration<Cobranza>
    {
        public CobranzaMap()
        {
            ToTable("Cobranzas");
            HasKey(o => o.Id);
            Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(p => p.Recibo).WithMany().HasForeignKey(t => t.ReciboId);
        }
    }
}
