namespace MegaCable.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class loco : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Abonados",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombres = c.String(),
                        ApPaterno = c.String(),
                        ApMaterno = c.String(),
                        DNI = c.String(),
                        Email = c.String(),
                        Telefono = c.String(),
                        Celular = c.String(),
                        Direccion = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Cobranzas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FechaPago = c.DateTime(nullable: false),
                        Pago = c.Boolean(nullable: false),
                        Mora = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ReciboId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Recibos", t => t.ReciboId, cascadeDelete: true)
                .Index(t => t.ReciboId);
            
            CreateTable(
                "dbo.Recibos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Codigo = c.String(),
                        FechaEmision = c.DateTime(nullable: false),
                        Cobranza = c.Boolean(nullable: false),
                        ContratoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contratos", t => t.ContratoId, cascadeDelete: true)
                .Index(t => t.ContratoId);
            
            CreateTable(
                "dbo.Contratos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FechaPago = c.Int(nullable: false),
                        CopiaDNI = c.Boolean(nullable: false),
                        ReciboLuzAgua = c.Boolean(nullable: false),
                        Observaciones = c.String(),
                        AbonadoId = c.Int(nullable: false),
                        PlanId = c.Int(nullable: false),
                        SectorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Abonados", t => t.AbonadoId, cascadeDelete: true)
                .ForeignKey("dbo.Planes", t => t.PlanId, cascadeDelete: true)
                .ForeignKey("dbo.Sectores", t => t.SectorId, cascadeDelete: true)
                .Index(t => t.AbonadoId)
                .Index(t => t.PlanId)
                .Index(t => t.SectorId);
            
            CreateTable(
                "dbo.Planes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombrePlan = c.String(),
                        VelocidadPlan = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValorPlan = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValorInstalacion = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalPago = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sectores",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombreSector = c.String(),
                        Direccion = c.String(),
                        ubigeoUnicoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ubigeos", t => t.ubigeoUnicoId, cascadeDelete: true)
                .Index(t => t.ubigeoUnicoId);
            
            CreateTable(
                "dbo.Ubigeos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Distrito = c.String(),
                        Provincia = c.String(),
                        Departamento = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cobranzas", "ReciboId", "dbo.Recibos");
            DropForeignKey("dbo.Recibos", "ContratoId", "dbo.Contratos");
            DropForeignKey("dbo.Contratos", "SectorId", "dbo.Sectores");
            DropForeignKey("dbo.Sectores", "ubigeoUnicoId", "dbo.Ubigeos");
            DropForeignKey("dbo.Contratos", "PlanId", "dbo.Planes");
            DropForeignKey("dbo.Contratos", "AbonadoId", "dbo.Abonados");
            DropIndex("dbo.Cobranzas", new[] { "ReciboId" });
            DropIndex("dbo.Recibos", new[] { "ContratoId" });
            DropIndex("dbo.Contratos", new[] { "SectorId" });
            DropIndex("dbo.Sectores", new[] { "ubigeoUnicoId" });
            DropIndex("dbo.Contratos", new[] { "PlanId" });
            DropIndex("dbo.Contratos", new[] { "AbonadoId" });
            DropTable("dbo.Ubigeos");
            DropTable("dbo.Sectores");
            DropTable("dbo.Planes");
            DropTable("dbo.Contratos");
            DropTable("dbo.Recibos");
            DropTable("dbo.Cobranzas");
            DropTable("dbo.Abonados");
        }
    }
}
