﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MegaCable.Web.Startup))]
namespace MegaCable.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
