﻿using MegaCable.Entities;
using MegaCable.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MegaCable.Web.Controllers
{
    public class ReciboController : Controller
    {
        private IContratoService contratoService;
        private IReciboService reciboService;

        public ReciboController(IContratoService contratoService, IReciboService reciboService)
        {
            this.contratoService = contratoService;
            this.reciboService = reciboService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var recibo = reciboService.GetRecibo();
            return View(recibo);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var contrato = contratoService.GetContrato();
            ViewBag.Contrato = new SelectList(contrato, "Id", "Abonado.DNI");
            return View();
        }

        [HttpPost]
        public ActionResult Create(Recibo recibo)
        {
            if (ModelState.IsValid)
            {
                reciboService.Guardar(recibo);
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var contrato = contratoService.GetContrato();
            ViewBag.Contrato = new SelectList(contrato, "Id", "Abonado.DNI");
            var recibo = reciboService.GetRecibo(id);
            return View(recibo);
        }

        [HttpPost]
        public ActionResult Edit(Recibo recibo)
        {
            if (ModelState.IsValid)
            {
                reciboService.Editar(recibo);
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}