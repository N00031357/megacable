﻿using MegaCable.Entities;
using MegaCable.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MegaCable.Web.Controllers
{
    public class ContratoController : Controller
    {
        private IContratoService contratoService;
        private IPlanService planService;
        private ISectorService sectorService;

        public ContratoController(IContratoService contratoService, 
            IPlanService planService, ISectorService sectorService)
        {
            this.contratoService = contratoService;
            this.planService = planService;
            this.sectorService = sectorService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var contrato = contratoService.GetContrato();
            return View(contrato);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var plan = planService.GetPlanes();
            ViewBag.Plan = new SelectList(plan, "Id", "NombrePlan");
            var sector = sectorService.GetSectores();
            ViewBag.Sector = new SelectList(sector, "Id", "NombreSector");
            return View();
        }

        [HttpPost]
        public ActionResult Create(Contrato contrato)
        {
            if (ModelState.IsValid)
            {
                contratoService.Guardar(contrato);
                return RedirectToAction("Index");
            }
            var plan = planService.GetPlanes();
            ViewBag.Plan = new SelectList(plan, "Id", "NombrePlan");
            var sector = sectorService.GetSectores();
            ViewBag.Sector = new SelectList(sector, "Id", "NombreSector");
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int id=0)
        {
            var plan = planService.GetPlanes();
            ViewBag.Plan = new SelectList(plan, "Id", "NombrePlan");
            var sector = sectorService.GetSectores();
            ViewBag.Sector = new SelectList(sector, "Id", "NombreSector");
            var contrato = contratoService.GetContrato(id);
            return View(contrato);
        }

        [HttpPost]
        public ActionResult Edit(Contrato contrato)
        {
            if (ModelState.IsValid)
            {
                contratoService.Editar(contrato);
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}