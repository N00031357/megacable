﻿using MegaCable.Entities;
using MegaCable.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MegaCable.Web.Controllers
{
    public class CobranzaController : Controller
    {
        private ICobranzaService cobranzaService;
        private IReciboService reciboService;

        public CobranzaController(ICobranzaService cobranzaService, IReciboService reciboService)
        {
            this.cobranzaService = cobranzaService;
            this.reciboService = reciboService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var cobranza = cobranzaService.GetCobranza();
            return View(cobranza);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var recibo = reciboService.GetRecibo();
            ViewBag.Recibo = new SelectList(recibo, "Id", "Contrato.Abonado.DNI");
            return View();
        }

        [HttpPost]
        public ActionResult Create(Cobranza cobranza)
        {
            if (ModelState.IsValid)
            {
                cobranzaService.Guardar(cobranza);
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var recibo = reciboService.GetRecibo();
            ViewBag.Recibo = new SelectList(recibo, "Id", "Contrato.Abonado.DNI");
            var cobranza = cobranzaService.GetCobranza(id);
            return View(cobranza);
        }

        [HttpPost]
        public ActionResult Edit(Cobranza cobranza)
        {
            if (ModelState.IsValid)
            {
                cobranzaService.Editar(cobranza);
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}