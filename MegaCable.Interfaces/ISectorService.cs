﻿using MegaCable.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Interfaces
{
    public interface ISectorService
    {
        List<Sector> GetSectores();
    }
}
