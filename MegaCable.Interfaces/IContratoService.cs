﻿using MegaCable.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Interfaces
{
    public interface IContratoService
    {
        Contrato GetContrato(int id);
        void Guardar(Contrato contrato);
        void Editar(Contrato contrato);
        IEnumerable<Contrato> GetContrato();
    }
}
