﻿using MegaCable.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Interfaces
{
    public interface ICobranzaService
    {
        Cobranza GetCobranza(int id);
        void Guardar(Cobranza cobranza);
        void Editar(Cobranza cobranza);
        IEnumerable<Cobranza> GetCobranza();
    }
}
