﻿using MegaCable.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Interfaces
{
    public interface IReciboService
    {
        Recibo GetRecibo(int id);
        void Guardar(Recibo recibo);
        void Editar(Recibo recibo);
        IEnumerable<Recibo> GetRecibo();
    }
}
