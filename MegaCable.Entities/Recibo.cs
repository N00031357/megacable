﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Entities
{
    public class Recibo
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public DateTime FechaEmision { get; set; }
        public bool Cobranza { get; set; }
        public virtual Contrato Contrato { get; set; }
        public int ContratoId { get; set; }
    }
}