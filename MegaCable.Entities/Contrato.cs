﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Entities
{
    public class Contrato
    {



        [Required]
        public int Id { get; set; }

        [Required]
        public int FechaPago { get; set; }

        [Required]
        public bool CopiaDNI { get; set; }

        [Required]
        public bool ReciboLuzAgua { get; set; }

        [Required]
        public string Observaciones { get; set; }


        [Required]
        public virtual Abonado Abonado { get; set; }
        [Required]
        public int AbonadoId { get; set; }

        [Required]
        public virtual Plan Plan { get; set; }

        [Required]
        public int PlanId { get; set; }

        [Required]
        public virtual Sector Sector { get; set; }

        [Required]
        public int SectorId { get; set; }
    }
}
