﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Entities
{
    public class Plan
    {
        public int Id { get; set; }
        public string NombrePlan { get; set; }
        public decimal VelocidadPlan { get; set; }
        public decimal ValorPlan { get; set; }
        public decimal ValorInstalacion { get; set; }
        public decimal TotalPago { get; set; }
    }
}
