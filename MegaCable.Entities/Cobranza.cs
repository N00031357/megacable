﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Entities
{
    public class Cobranza
    {
        public int Id { get; set; }
        public DateTime FechaPago { get; set; }
        public bool Pago { get; set; }
        public decimal Mora { get; set; }
        public virtual Recibo Recibo { get; set; }
        public int ReciboId { get; set; }
    }
}
