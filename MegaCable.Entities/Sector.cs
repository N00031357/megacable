﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaCable.Entities
{
    public class Sector
    {
        public int Id { get; set; }
        public string NombreSector { get; set; }
        public string Direccion { get; set; }
        public virtual UbigeoUnico UbigeoUnico { get; set; }
        public int ubigeoUnicoId { get; set; }
    }
}
