﻿using MegaCable.Entities;
using MegaCable.Interfaces;
using MegaCable.Services;
using MegaCable.Web.Controllers;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MegaCable.Tests.Controller
{
    [TestFixture]
    class ContratoTest
    {

        Mock<IContratoService> _sContratoFalse;
        Mock<IPlanService> _sPlanFalse;
        Mock<ISectorService> _sSectorFalse;

        [SetUp]
        public void Iniciar()
        {
            _sContratoFalse = new Mock<IContratoService>();
            _sPlanFalse = new Mock<IPlanService>();
            _sSectorFalse = new Mock<ISectorService>();

            _sPlanFalse.Setup(o => o.GetPlanes()).Returns(
                new List<Plan>()
                {
                    new Plan { },
                    new Plan { }
                }
            );

            _sSectorFalse.Setup(o => o.GetSectores()).Returns(
                new List<Sector>()
                {
                    new Sector { },
                    new Sector { }
                }
            );
        }


        [Test]
        public void GuardarExitosoContrato()
        {
            var controller = new ContratoController(
                _sContratoFalse.Object,
                _sPlanFalse.Object,
                _sSectorFalse.Object
            );

            Contrato contrato = new Contrato
            {
                FechaPago = 20,
                CopiaDNI = true,
                ReciboLuzAgua = true,
                Observaciones = "Hola",
                AbonadoId = 5,
                PlanId = 1,
                SectorId = 1
            };

            controller.Create(
                contrato
            );

            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void ListaContrato()
        {
            _sContratoFalse.Setup(o => o. GetContrato() ).Returns(
                new List<Contrato>()
                {
                    new Contrato {
                    },
                    new Contrato {
                    },
                    new Contrato {
                    }
                }
            );

            var controller = new ContratoController(
                _sContratoFalse.Object,
                _sPlanFalse.Object,
                _sSectorFalse.Object
            );
            var resultado = controller.Index() as ViewResult;
            Assert.AreEqual(3, (resultado.Model as List<Contrato>).Count());

        }



    }
}
